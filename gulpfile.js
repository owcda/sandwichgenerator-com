var gulp = require('gulp');
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var plumber = require('gulp-plumber');

gulp.task('default', function () {
    return gulp.src('scss/**/*.scss')
			.pipe(sass())
			.pipe(gulp.dest('./css/'));
});