<!DOCTYPE html>
<html>
<head>
    <title>Sandwich Generator</title>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width">
    <meta name="description" content="When it's too damn hard to make up your mind about your own damn sandwich at Love Food Café.">
    <meta property="og:image" content="http://www.sandwichgenerator.com/img/share-SG.png">
    <link rel="stylesheet" href="libs/normalize.css"/>
    <link rel="stylesheet" href="css/main.css"/>
    <link href='https://fonts.googleapis.com/css?family=Arvo:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
</head>
<body>

<header>
  <div><img src="img/sg-header.svg" alt="logo"></div>
  <p>When it's too damn hard to make up your mind about your own damn sandwich at Love Food Café.</p>
</header>

<section>
  <h2>Here's your damn sandwich</h2>

  <ul class="ingredients animate">
  </ul>

  <button>Generate sandwich</button>

  <div class="options">
    <input type="checkbox" name="veggie" />
    <label><span>Only veggie stuff, please</span></label>
  </div>

  <img src="img/line.svg" alt="seperator line">
</section>

<footer>
  <a href="http://oakwood.se/" target="_blank">
    <img src="img/oakwood.svg" alt="Oakwood Creative Logo">
  </a>
</footer>

<script type="text/template" class="template">
  <% _.each(items, function(category, key, list) { %>
    <li><%= category[_.random(category.length-1)].title %></li>
  <% }); %>
</script>

<script src="js/ingredients.js"></script>
<script src="libs/jquery-2.0.3.js"></script>
<script src="libs/underscore-1.6.0.js"></script>
<script src="js/main.js"></script>
</body>
</html>