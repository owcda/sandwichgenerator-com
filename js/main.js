var love = window.love || {};

$(function (){

  var onlyVeggie = false;
  var ingredients = _.clone(love.ingredients);
  listTemplate = $('.template').html();

  function renderNewSandwich(){
    var $output = $(".ingredients");

    $output.html(_.template(listTemplate, { items: ingredients } ));

    /**
     * todo: refactor
     */
    $output.removeClass('animate');
    $output.find('li').css('opacity', '0');

    setTimeout(function(){
      $output.css({
        'background-image': 'url(img/brain.gif)'
      });
    }, 50);

    setTimeout(function(){
      $output.css('background-image', 'none');
      $output.removeClass('thinking').addClass('animate');
      $output.find('li').css('opacity', '1');
    }, 700);

  }

  /**
   * Checkbox for veggie stuff only
   */
  $('input[name="veggie"]').change(function() {
    onlyVeggie = this.checked;

    ingredients.mains = _.filter(love.ingredients.mains, function(thing) {
      return thing.veggie >= onlyVeggie;
    });

    renderNewSandwich();
  });

  /**
   * Generate Button
   */
  $('button').on('click', function(){
    renderNewSandwich();
  });

  renderNewSandwich();
});