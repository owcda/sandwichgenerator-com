var love = window.love || {};

love.ingredients = {
  'mains': [
    { 'title': 'Porkbelly', 'veggie': 0, 'hotness': 0 },
    { 'title': 'Pulled Prok', 'veggie': 0, 'hotness': 0 },
    { 'title': 'Beef Brisked', 'veggie': 0, 'hotness': 0 },
    { 'title': 'Korean BBQ Chick’n', 'veggie': 0, 'hotness': 0 },
    { 'title': 'Pulled Beef Adobo ', 'veggie': 0, 'hotness': 0 },
    { 'title': 'Veggie Slider (V)', 'veggie': 1, 'hotness': 0 },
    { 'title': 'Beats N’ Beans Slider (V)', 'veggie': 1, 'hotness': 0 },
    { 'title': 'Korean BBQ Tofu (VV)', 'veggie': 2, 'hotness': 0 },
  ],
  'sauces': [
    { 'title': 'Smokey BBQ', 'veggie': 0, 'hotness': 0 },
    { 'title': 'Blue Cheese', 'veggie': 0, 'hotness': 0 },
    { 'title': 'Tangy Mustard', 'veggie': 0, 'hotness': 0 },
    { 'title': 'Green Goddess', 'veggie': 0, 'hotness': 0 },
    { 'title': 'Creamy Rooster (X)', 'veggie': 0, 'hotness': 1 },
    { 'title': 'Chimichurri (X)', 'veggie': 0, 'hotness': 1 },
    { 'title': 'Hot Lemon n’ Garlic (X)', 'veggie': 0, 'hotness': 0 },
    { 'title': 'Hot Sauce (XX)', 'veggie': 0, 'hotness': 2 },
    { 'title': 'Hot Jalapeno (XXX)', 'veggie': 0, 'hotness': 3 },
  ],
  'topping': [
    { 'title': 'Green Goddess Slaw', 'veggie': 0, 'hotness': 0 },
    { 'title': 'CaesarSlaw', 'veggie': 0, 'hotness': 0 },
    { 'title': 'Street Salad', 'veggie': 0, 'hotness': 0 },
    { 'title': 'Rawslaw ', 'veggie': 0, 'hotness': 0 },
    { 'title': 'Coleslaw', 'veggie': 0, 'hotness': 0 },
    { 'title': 'Sauerkraut', 'veggie': 0, 'hotness': 0 },
    { 'title': 'Fried egg', 'veggie': 0, 'hotness': 0 },
  ],
  'personal': [
    { 'title': 'Chow Chow', 'veggie': 0, 'hotness': 0 },
    { 'title': 'Pickled Red Onion ', 'veggie': 0, 'hotness': 0 },
    { 'title': 'Pickled Jalapeno ', 'veggie': 0, 'hotness': 0 },
    { 'title': 'Root Crisps', 'veggie': 0, 'hotness': 0 },
    { 'title': 'Doritos', 'veggie': 0, 'hotness': 0 },
    { 'title': 'Cilantro', 'veggie': 0, 'hotness': 0 },
  ],
  'something': [
    { 'title': 'Sweet Potato Salad', 'veggie': 0, 'hotness': 0 },
    { 'title': 'Heirloom Bean Salad ', 'veggie': 0, 'hotness': 0 },
    { 'title': 'Chick’n Soup ', 'veggie': 0, 'hotness': 0 },
    { 'title': 'Fries', 'veggie': 0, 'hotness': 0 },
    { 'title': 'Slaw of your choice', 'veggie': 0, 'hotness': 0 },
  ]
};